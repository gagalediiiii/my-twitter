import Api from "@/api";

export default {
  async createPost({ message, author }) {
    const response = await Api.post("/posts", {
      message,
      author
    });
    return response.data;
  },

  async getPosts() {
    const response = await Api.get("/posts");
    return response.data;
  }
};
