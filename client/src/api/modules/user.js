import Api from "@/api";

export default {
  async register({ nickname, password }) {
    const response = await Api.post("/register", { nickname, password });
    return response.data;
  },

  async login({ nickname, password }) {
    const response = await Api.post("/login", { nickname, password });
    return response.data;
  },

  async getUser(id) {
    const response = await Api.get(`/users/${id}`);
    return response.data;
  }
};
