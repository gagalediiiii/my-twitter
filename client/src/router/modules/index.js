import auth from "./auth";

export default [
  {
    path: "*",
    redirect: "/"
  },
  {
    path: "/",
    name: "home",
    component: () => import("@/views/Home.vue")
  },

  ...auth
];
