export default [
  {
    path: "/sign-in",
    name: "sign-in",
    component: () =>
      import(/* webpackChunkName: "SignIn" */ "@/views/auth/SignIn")
  },
  {
    path: "/sign-up",
    name: "sign-up",
    component: () =>
      import(/* webpackChunkName: "SignUp" */ "@/views/auth/SignUp")
  }
];
