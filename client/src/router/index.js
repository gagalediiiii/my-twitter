import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./modules";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const userIsAuthenticated = localStorage.getItem("userId");

  if (
    !userIsAuthenticated &&
    (to.name === "sign-in" || to.name === "sign-up")
  ) {
    return next();
  }

  if (!userIsAuthenticated) {
    return next({ name: "sign-in" });
  }

  if (userIsAuthenticated && (to.name === "sign-in" || to.name === "sign-up")) {
    return next({ name: "home" });
  }

  next();
});

export default router;
