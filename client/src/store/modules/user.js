import UserProvider from "@/api/modules/user";
import { mutationTypesEnum } from "../types";

const state = () => ({
  user: {}
});

const getters = {
  me: state => state.user
};

const actions = {
  async register({ commit }, form) {
    const user = await UserProvider.register(form);

    localStorage.setItem("userId", user._id);
    commit(mutationTypesEnum.INIT_USER, user);
    return user;
  },

  async login({ commit }, form) {
    const user = await UserProvider.login(form);

    localStorage.setItem("userId", user._id);
    commit(mutationTypesEnum.INIT_USER, user);
    return user;
  },

  logout({ commit }) {
    localStorage.removeItem("userId");
    commit(mutationTypesEnum.INIT_USER, {});
  },

  async getUser({ commit }, id) {
    const user = await UserProvider.getUser(id);
    commit(mutationTypesEnum.INIT_USER, user);
    return user;
  }
};

const mutations = {
  [mutationTypesEnum.INIT_USER](state, user) {
    state.user = user;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
