import PostsProvider from "@/api/modules/posts";
import { mutationTypesEnum } from "../types";

const state = () => ({
  posts: {}
});

const getters = {
  posts: state => state.posts
};

const actions = {
  async createPost({ commit }, params) {
    const post = await PostsProvider.createPost(params);
    commit(mutationTypesEnum.ADD_POST, post);
    return post;
  },

  async getPosts({ commit }) {
    const posts = await PostsProvider.getPosts();
    commit(mutationTypesEnum.INIT_POSTS, posts);
    return posts;
  }
};

const mutations = {
  [mutationTypesEnum.INIT_POSTS](state, posts) {
    state.posts = posts;
  },

  [mutationTypesEnum.ADD_POST](state, post) {
    state.posts.unshift(post);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
