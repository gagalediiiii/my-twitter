import Vue from "vue";
import Vuex from "vuex";
import modules from "./modules";

Vue.use(Vuex);

// TODO: implement models for post and user

export default new Vuex.Store({
  modules,
  strict: process.env.NODE_ENV !== "production"
});
