export const mutationTypesEnum = Object.freeze({
  INIT_USER: "INIT_USER",
  INIT_POSTS: "INIT_POSTS",
  ADD_POST: "ADD_POST"
});
