const ERRORS = {
  "0001": "Something went wrong.",
  "0002": "Incorrect data.",
  "0003": "Nickname must have at least 4 letters.",
  "0004": "Password must have at least 8 letters."
};

/**
 * searchError
 *
 * @param errCode
 * @param cb
 */
export function searchError(errCode, cb) {
  const reason = ERRORS[errCode] || ERRORS["0001"];
  cb(reason);
}

/**
 * validateNickname
 *
 * @param name
 */
export function validateNickname(name) {
  const re = new RegExp("^(?=[a-zA-Z0-9._]{4,20}$)(?!.*[_.]{2})[^_.].*[^_.]$");
  return re.test(name);
}
