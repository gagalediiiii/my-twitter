import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "./plugins";

Vue.config.productionTip = false;

// TODO: implement internationalization

// TODO: Implement proper error handling on high level api methods usages,
// create custom error classes

// TODO: implement preloaders

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
