# My Twitter

## To review the project do the following steps:

### 1. Start server

```
cd server
npm install
npm run serve
```

### 2. Start client

```
cd client
npm install
npm run serve
```
Open the link from terminal, by default it's http://localhost:8080/

There is no need to raise the database, since the project is connected to MongoDB Atlas.