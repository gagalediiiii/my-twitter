const { GeneralError } = require("../utils/errors");

const errorHandler = (err, req, res, next) => {
  console.error("err stack", err.stack);

  if (err instanceof GeneralError) {
    return res.status(err.getCode()).json({
      status: "error",
      error: err.name,
      message: err.message
    });
  }

  if (err.name === "ValidationError") {
    return res.status(422).json({
      status: "error",
      error: err.name,
      message: err.message
    });
  }

  return res.status(500).json({
    status: "error",
    error: err.name,
    message: err.message
  });
};


module.exports = errorHandler;