const { Schema, model } = require("mongoose");

const postSchema = new Schema(
  {
    message: {
      type: String,
      required: [true, "message field is required"],
    },
    author: {
      type: Schema.Types.ObjectId,
      ref: "UserModel",
      required: [true, "author field is required"],
    }
  },
  { timestamps: true, collection: "Posts" }
);

module.exports = model("PostModel", postSchema);
