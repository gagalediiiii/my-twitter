const { Schema, model } = require("mongoose");
const bcrypt = require("bcrypt-nodejs");

const userSchema = new Schema(
  {
    nickname: {
      type: String,
      required: [true, "nickname field is required"],
    },
    passwordHash: {
      type: String,
      required: [true, "password field is required"],
      minlength: 8,
    },
  },
  { timestamps: true, collection: "Users" }
);

userSchema
  .virtual("password")
  .set(function (password) {
    this.passwordHash =
      password.length === 0
        ? ""
        : password.length < 8
        ? "error"
        : this.encryptPassword(password);
  })
  .get(() => "secure password");

userSchema.methods = {
  encryptPassword: function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
  },
  checkPassword: function (password) {
    return bcrypt.compareSync(password, this.passwordHash);
  },
};

module.exports = model("UserModel", userSchema);
