const express = require("express");
const { asyncHandler } = require("../middlewares");
const { createPost, readPosts } = require("../controllers/post.controller");

const router = express.Router();

router
  .post("/posts", asyncHandler(createPost))
  .get("/posts", asyncHandler(readPosts));

module.exports = router;
