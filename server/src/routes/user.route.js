const express = require("express");
const { asyncHandler } = require("../middlewares");
const {
  registerUser,
  loginUser,
  readUser,
} = require("../controllers/user.controller");

const router = express.Router();

router
  .post("/register", asyncHandler(registerUser))
  .post("/login", asyncHandler(loginUser))
  .get("/users/:id", asyncHandler(readUser));

module.exports = router;
