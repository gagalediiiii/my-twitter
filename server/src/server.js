// Importing required modules
const cors = require("cors");
const express = require("express");
const { errorHandler } = require("./middlewares");

// parse env variables
require("dotenv").config();

// connect mongodb
require("./db").connect();

const app = express();

// Configure middlewares
app.use(cors());
app.use(express.json());

// app.set("view engine", "html");

// // Static folder
// app.use(express.static(__dirname + "/views/"));

// Defining route middleware
app.use("/api", [
  require("./routes/user.route"),
  require("./routes/post.route"),
]);

// Defining error handler middleware
app.use(errorHandler);

app.get("*", (req, res) => res.send("Sorry can't find that!", 404));

// Listening to port
app.listen(process.env.PORT, () => {
  console.log(`Listening On http://localhost:${process.env.PORT}/api`);
});

module.exports = app;
