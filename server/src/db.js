const mongoose = require("mongoose");

class Db {
  constructor(params) {
    this.instance = null;
    this.params = params;

    this.connection = mongoose.connection;
    this.connection.on("connected", () => console.log("DB connected"));
    this.connection.on("disconnected", () => console.log("DB disconnected"));
    this.connection.on("reconnect", () => console.log("DB reconnected"));
  }

  async connect() {
    try {
      this.instance = await mongoose.connect(this.params.dbUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      });
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async disconnect(fn) {
    await mongoose.disconnect(fn);
    console.log("DB: connection closed");
  }
}

module.exports = new Db({ dbUrl: process.env.DB_URL });
