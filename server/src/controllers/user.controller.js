const User = require("../models/user.model");
const ObjectId = require("mongoose").Types.ObjectId;
const {
  NotFoundError,
  BadRequestError,
  PropertyRequiredError,
  UnauthorizedError,
  ConflictError
} = require("../utils/errors");

const readUser = async (req, res) => {
  const userId = req.params.id;

  if (!userId) throw new PropertyRequiredError("id");

  if (!ObjectId.isValid(userId)) {
    throw new BadRequestError("Invalid user id property");
  }

  const user = await User.findById(userId);

  if (!user) {
    throw new NotFoundError("User not found");
  }

  res.status(200).json(user);
};

const registerUser = async (req, res) => {
  const { nickname, password } = req.body;

  if (!nickname) throw new PropertyRequiredError("nickname");

  if (!password) throw new PropertyRequiredError("password");

  const userExist = await User.exists({ nickname: req.body.nickname });

  if (userExist) throw new ConflictError("This nickname is already taken");

  const user = await User.create(req.body);

  res.status(201).json(user);
};

const loginUser = async (req, res, next) => {
  const { nickname, password } = req.body;

  if (!nickname) throw new PropertyRequiredError("nickname");

  if (!password) throw new PropertyRequiredError("password");

  const user = await User.findOne({ nickname });

  if (!user) throw new NotFoundError("User not found");

  if (!user.checkPassword(password)) {
    throw new UnauthorizedError("Password incorrect");
  }

  res.status(201).json(user);
};

module.exports = {
  readUser,
  registerUser,
  loginUser,
};
