const Post = require("../models/post.model");
const ObjectId = require("mongoose").Types.ObjectId;
const {
  BadRequestError,
  PropertyRequiredError,
} = require("../utils/errors");

const createPost = async (req, res) => {
  const { message, author } = req.body;

  if (!message) throw new PropertyRequiredError("message");

  if (!author) throw new PropertyRequiredError("author");

  if (!ObjectId.isValid(author)) {
    throw new BadRequestError("Invalid author property");
  }

  const post = await Post.create(req.body);
  res.status(201).json(post);
};

const readPosts = async (req, res) => {
  const limit = 10;
  const today = new Date();

  const postsByDate = await Post.find({
    createdAt: {
      $gte: today.setHours(0,0,0,0),
      $lt: today.setHours(23,59,59,999)
    },
  }).populate("author");

  const shuffled = [...postsByDate].sort(() => 0.5 - Math.random());
  const postsByDateRandom = shuffled.slice(0, limit);

  res.status(200).json(postsByDateRandom);
};

module.exports = {
  createPost,
  readPosts,
};
