class GeneralError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
  }

  getCode() {
    if (this instanceof BadRequestError || this instanceof PropertyRequiredError) {
      return 400;
    }

    if (this instanceof NotFoundError) return 404;

    if (this instanceof UnauthorizedError) return 401;

    if (this instanceof ConflictError) return 409;

    return 500;
  }
}

class PropertyRequiredError extends GeneralError {
  constructor(property) {
    super(`This field is required: ${property}`);
    this.property = property;
  }
}

class BadRequestError extends GeneralError {}
class NotFoundError extends GeneralError {}
class UnauthorizedError extends GeneralError {}
class ConflictError extends GeneralError {}

module.exports = {
  GeneralError,
  PropertyRequiredError,
  BadRequestError,
  NotFoundError,
  UnauthorizedError,
  ConflictError
};